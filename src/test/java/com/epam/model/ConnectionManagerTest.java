package com.epam.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;

import java.sql.Connection;
import java.sql.SQLException;

import static junit.framework.TestCase.*;

public class ConnectionManagerTest {
    private static final Logger LOGGER = LogManager.getLogger(ConnectionManagerTest.class);

    @Test
    @DisplayName("Testing Connection")
    public void getConnectionTest(){
        Connection connection = ConnectionManager.getConnection();
        try {
            assertNotNull(connection);
            connection.close();
            System.out.println("All gut");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    @DisplayName("Testing the name of the DB")
    public void getDBNameTest(){
        Connection connection = ConnectionManager.getConnection();
        try {
            assertEquals("rozetka", connection.getCatalog());
            System.out.println("All gut");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        new ConnectionManagerTest().getDBNameTest();
        new ConnectionManagerTest().getConnectionTest();
    }
}
