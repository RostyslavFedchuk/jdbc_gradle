package com.epam;

import com.epam.dao.implementations.controller.CategoryDAOImplTest;
import com.epam.model.ConnectionManagerTest;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.platform.suite.api.IncludeClassNamePatterns;
import org.junit.platform.suite.api.SelectPackages;
import org.junit.runner.RunWith;

@RunWith(JUnitPlatform.class)
@DisplayName("All tests")
@SelectPackages("com.epam")
@IncludeClassNamePatterns({"^.*$"})
public class AllTest {
    @Test
    public static void main(String[] args) {
        ConnectionManagerTest manager = new ConnectionManagerTest();
        manager.getDBNameTest();
        manager.getConnectionTest();
        new CategoryDAOImplTest().insertDeleteTest();
    }
}
