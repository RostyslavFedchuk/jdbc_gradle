package com.epam.model.tables;

import com.epam.model.annotations.Column;
import com.epam.model.annotations.PrimaryKey;
import com.epam.model.annotations.Table;

@Table(name = "Category")
public class Category {
    @PrimaryKey
    @Column(name = "id")
    private Integer id;
    @Column(name = "name")
    private String name;
    @Column(name = "description")
    private String description;

    public Category(){}

    public Category(Integer id, String name, String description){
        this.id = id;
        this.name = name;
        this.description = description;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getColumns(){
        return String.format("%-7s %-20s %-20s\n", "id", "name", "description");
    }

    @Override
    public String toString() {
        return String.format("%-7d %-20s %-20s", id, name, description);
    }
}
