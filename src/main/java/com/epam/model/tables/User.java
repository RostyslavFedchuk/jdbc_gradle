package com.epam.model.tables;

import com.epam.model.annotations.*;

@Table(name = "User")
public class User {
    @PrimaryKey
    @Column(name = "id")
    private Integer id;
    @Column(name = "login")
    private String login;
    @Column(name = "name")
    private String name;
    @Column(name = "Surname")
    private String surname;
    @Column(name = "gender")
    private String gender;
    @Column(name = "email")
    private String email;
    @Column(name = "phone_number")
    private String phoneNumber;
    @Column(name = "address")
    private String address;
    @Column(name = "user_password_id")
    private Integer userPasswordId;

    public User(){}

    public User(Integer id, String login, String name, String surname, String gender,
                String email, String phoneNumber, String address, Integer userPasswordId) {
        this.id = id;
        this.login = login;
        this.name = name;
        this.surname = surname;
        this.gender = gender;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.address = address;
        this.userPasswordId = userPasswordId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getUserPasswordId() {
        return userPasswordId;
    }

    public void setUserPasswordId(Integer userPasswordId) {
        this.userPasswordId = userPasswordId;
    }

    public String getColumns(){
        return String.format("%-7s %-15s %-15s %-15s %-10s %-20s %-20s %-30s %s",
                "id", "login", "name", "surname", "gender", "email", "phoneNumber",
                "address", "userPasswordId");
    }

    @Override
    public String toString() {
        return String.format("%-7d %-15s %-15s %-15s %-10s %-20s %-20s %-30s %d",
                id, login, name, surname, gender, email, phoneNumber, address, userPasswordId);
    }
}
