package com.epam.model.tables;

import com.epam.model.annotations.*;

@Table(name = "Product")
public class Product {
    @PrimaryKey
    @Column(name = "id")
    private Integer id;
    @Column(name = "bar_code")
    private Integer barCode;
    @Column(name = "category_id")
    private Integer categoryId;
    @Column(name = "production")
    private String production;

    public Product(){}

    public Product(Integer id, Integer barCode, Integer categoryId, String production) {
        this.id = id;
        this.barCode = barCode;
        this.categoryId = categoryId;
        this.production = production;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getBarCode() {
        return barCode;
    }

    public void setBarCode(Integer barCode) {
        this.barCode = barCode;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public String getProduction() {
        return production;
    }

    public void setProduction(String production) {
        this.production = production;
    }

    public String getColumns(){
        return String.format("%-7s %-15s %-15s %-20s",
                "id", "barCode", "categoryId", "production");
    }

    @Override
    public String toString() {
        return String.format("%-7d %-15d %-15d %-20s",
                id, barCode, categoryId, production);
    }
}
