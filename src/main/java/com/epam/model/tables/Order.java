package com.epam.model.tables;

import com.epam.model.annotations.Column;
import com.epam.model.annotations.PrimaryKey;
import com.epam.model.annotations.Table;

import java.sql.Date;

@Table(name = "Order")
public class Order {
    @PrimaryKey
    @Column(name = "id")
    private Integer id;
    @Column(name = "date")
    private Date date;
    @Column(name = "details")
    private String details;
    @Column(name = "status_code")
    private Integer statusCode;
    @Column(name = "user_id")
    private Integer UserId;
    @Column(name = "Payment_id")
    private Integer PaymentId;

    public Order(){}

    public Order(Integer id, Date date, String details,
                 Integer statusCode, Integer UserId, Integer PaymentId){
        this.id = id;
        this.date = date;
        this.details = details;
        this.statusCode = statusCode;
        this.UserId = UserId;
        this.PaymentId = PaymentId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public Integer getUserId() {
        return UserId;
    }

    public void setUserId(Integer userId) {
        UserId = userId;
    }

    public Integer getPaymentId() {
        return PaymentId;
    }

    public void setPaymentId(Integer paymentId) {
        PaymentId = paymentId;
    }

    public String getColumns(){
        return String.format("%-7s %-15s %-20s %-5s %-5s %-5s",
                "id", "date", "details", "statusCode", "UserId", "PaymentId");
    }

    @Override
    public String toString() {
        return String.format("%-7d %-15s %-20s %-5d %-5d %-5d",
                id, date, details, statusCode, UserId, PaymentId);
    }
}
