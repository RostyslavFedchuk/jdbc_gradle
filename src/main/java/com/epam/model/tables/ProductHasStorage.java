package com.epam.model.tables;

import com.epam.model.annotations.*;

@Table(name = "Product_has_Storage")
public class ProductHasStorage {
    @PrimaryKey
    @Column(name = "id")
    private Integer id;
    @Column(name = "product_id")
    private Integer productId;
    @Column(name = "storage_id")
    private Integer storageId;
    @Column(name = "count")
    private Integer count;

    public ProductHasStorage(){}

    public ProductHasStorage(Integer id, Integer productId, Integer storageId, Integer count) {
        this.id = id;
        this.productId = productId;
        this.storageId = storageId;
        this.count = count;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Integer getStorageId() {
        return storageId;
    }

    public void setStorageId(Integer storageId) {
        this.storageId = storageId;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public String getColumns(){
        return String.format("%-7s %-7s %-7s %-10s", "id", "productId", "storageId", "count");
    }

    @Override
    public String toString() {
        return String.format("%-7d %-7d %-7d %-10d", id, productId, storageId, count);
    }
}
