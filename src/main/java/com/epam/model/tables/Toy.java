package com.epam.model.tables;

import com.epam.model.annotations.*;

@Table(name = "Toy")
public class Toy {
    @PrimaryKey
    @Column(name = "bar_code")
    private Integer barCode;
    @Column(name = "name")
    private String name;
    @Column(name = "color")
    private String color;
    @Column(name = "price")
    private Double price;

    public Toy(){}

    public Toy(Integer barCode, String name, String color, Double price) {
        this.barCode = barCode;
        this.name = name;
        this.color = color;
        this.price = price;
    }

    public Integer getBarCode() {
        return barCode;
    }

    public void setBarCode(Integer barCode) {
        this.barCode = barCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getColumns(){
        return String.format("%-7s %-20s %-20s %-15s", "barCode", "name", "color", "price");
    }

    @Override
    public String toString() {
        return String.format("%-7d %-20s %-20s %-15s", barCode, name, color, price);
    }
}
