package com.epam.model.tables;

import com.epam.model.annotations.*;

@Table(name = "Storage")
public class Storage {
    @PrimaryKey
    @Column(name = "id")
    private Integer id;
    @Column(name = "name")
    private String name;
    @Column(name = "Address")
    private String address;

    public Storage(){}

    public Storage(Integer id, String name, String address) {
        this.id = id;
        this.name = name;
        this.address = address;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getColumns(){
        return String.format("%-7s %-20s %-30s", "id", "name", "address");
    }

    @Override
    public String toString() {
        return String.format("%-7d %-20s %-30s", id, name, address);
    }
}
