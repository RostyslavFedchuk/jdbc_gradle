package com.epam.model.structure;

public class ColumnStructure {
    private String name;
    private String type;
    private String size;
    private boolean isPrimaryKey;
    private boolean isForeignKey;
    private boolean isNull;
    private boolean isAutoincrement;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public boolean isPrimaryKey() {
        return isPrimaryKey;
    }

    public void setPrimaryKey(boolean primaryKey) {
        isPrimaryKey = primaryKey;
    }

    public boolean isForeignKey() {
        return isForeignKey;
    }

    public void setForeignKey(boolean foreignKey) {
        isForeignKey = foreignKey;
    }

    public boolean isNull() {
        return isNull;
    }

    public void setNull(boolean aNull) {
        isNull = aNull;
    }

    public boolean isAutoincrement() {
        return isAutoincrement;
    }

    public void setAutoincrement(boolean autoincrement) {
        isAutoincrement = autoincrement;
    }

    @Override
    public String toString() {
        return String.format("%20s %15s(%s) %5s %5s %10s %15s",
                name, type, size, isPrimaryKey ? "PK" : "", isForeignKey ? "FK" : "",
                isNull ? "NULL" : "NOT NULL", isAutoincrement ? "Autoincrement" : "");
    }
}
