package com.epam.model.structure;

import java.util.ArrayList;
import java.util.List;

public class TableStructure {
    private String name;
    private List<ColumnStructure> columns;

    {
        columns = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ColumnStructure> getColumns() {
        return columns;
    }

    public void setColumns(List<ColumnStructure> columns) {
        this.columns = columns;
    }

    @Override
    public String toString() {
        String result = "TABLE " + name + " (\n";
        for (ColumnStructure column : columns) {
            result += column.toString() + "\n";
        }
        result+=")\n";
        return result;
    }
}
