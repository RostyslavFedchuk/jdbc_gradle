package com.epam;

import com.epam.view.MyView;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.SQLException;

public class Main {
    private static final Logger LOGGER = LogManager.getLogger(MyView.class);

    public static void main(String[] args) {
        try {
            new MyView().show();
        } catch (SQLException e) {
            LOGGER.warn(e.getMessage());
        }
    }
}
