package com.epam.view;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.ResourceBundle;

class Menu {
    private final int COUNT_OPTIONS;
    private Map<String, String> menu;

    private ResourceBundle bundle;

    Menu(String bundleName) {
        bundle = ResourceBundle.getBundle(bundleName);
        COUNT_OPTIONS = Integer.parseInt(bundle.getString("countOptions"));
        setMenu();
    }

    private void setMenu(){
        menu = new LinkedHashMap<>();
        for (int i = 1; i <= COUNT_OPTIONS; i++) {
            menu.put("" + i, bundle.getString("" + i));
        }
        menu.put("Q", bundle.getString("Q"));
    }

    Map<String, String> getMenu(){
        return menu;
    }
}
