package com.epam.controller.dao.interfaces;

import java.sql.SQLException;
import java.util.List;
import java.util.ResourceBundle;

public interface GeneralDAO<T> {
    ResourceBundle bundle = ResourceBundle.getBundle("config");

    List<T> findAll() throws SQLException;

    int create(T object) throws SQLException;

    int update(T object) throws SQLException;

    int delete(Integer id) throws SQLException;
}
