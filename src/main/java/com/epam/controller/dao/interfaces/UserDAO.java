package com.epam.controller.dao.interfaces;

import com.epam.model.tables.User;

public interface UserDAO extends GeneralDAO<User> {
}
