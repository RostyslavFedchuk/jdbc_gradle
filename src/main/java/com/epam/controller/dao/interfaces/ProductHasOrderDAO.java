package com.epam.controller.dao.interfaces;

import com.epam.model.tables.ProductHasOrder;

import java.sql.SQLException;

public interface ProductHasOrderDAO extends GeneralDAO<ProductHasOrder> {
    int deleteProductsByOrder(Integer id) throws SQLException;
}
