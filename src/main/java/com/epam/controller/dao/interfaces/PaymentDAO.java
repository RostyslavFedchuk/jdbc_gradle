package com.epam.controller.dao.interfaces;

import com.epam.model.tables.Payment;

public interface PaymentDAO extends GeneralDAO<Payment>{
}
