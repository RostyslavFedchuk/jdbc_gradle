package com.epam.controller.dao.interfaces;

import com.epam.model.tables.Password;

public interface PasswordDAO extends GeneralDAO<Password>{
}
