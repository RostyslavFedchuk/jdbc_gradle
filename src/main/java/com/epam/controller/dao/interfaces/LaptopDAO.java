package com.epam.controller.dao.interfaces;

import com.epam.model.tables.Laptop;

import java.sql.SQLException;
import java.util.List;

public interface LaptopDAO extends GeneralDAO<Laptop> {
    List<Laptop> showByPriceRange(double begin, double end) throws SQLException;
}
