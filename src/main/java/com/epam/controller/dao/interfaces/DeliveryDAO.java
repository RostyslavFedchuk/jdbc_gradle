package com.epam.controller.dao.interfaces;

import com.epam.model.tables.Delivery;

public interface DeliveryDAO extends GeneralDAO<Delivery> {
}
