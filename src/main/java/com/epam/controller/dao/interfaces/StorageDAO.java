package com.epam.controller.dao.interfaces;

import com.epam.model.tables.Storage;

import java.sql.SQLException;

public interface StorageDAO extends GeneralDAO<Storage> {
    int deleteStorage(Integer toDelete, Integer toPaste) throws SQLException;
}
