package com.epam.controller.dao.interfaces;

import com.epam.model.tables.Product;

import java.sql.SQLException;

public interface ProductDAO extends GeneralDAO<Product> {
    void joinTables() throws SQLException;
}
