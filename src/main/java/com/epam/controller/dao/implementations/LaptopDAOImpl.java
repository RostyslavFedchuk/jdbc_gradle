package com.epam.controller.dao.implementations;

import com.epam.controller.Transformer;
import com.epam.controller.dao.interfaces.LaptopDAO;
import com.epam.model.ConnectionManager;
import com.epam.model.tables.Laptop;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class LaptopDAOImpl implements LaptopDAO {
    private static final String FIND_ALL;
    private static final String DELETE;
    private static final String CREATE;
    private static final String UPDATE;
    private static final String SEARCH_BY_RANGE;

    static {
        FIND_ALL = bundle.getString("FIND_ALL_Laptop");
        DELETE = bundle.getString("DELETE_Laptop");
        CREATE = bundle.getString("CREATE_Laptop");
        UPDATE = bundle.getString("UPDATE_Laptop");
        SEARCH_BY_RANGE = bundle.getString("SEARCH_BY_RANGE");
    }

    @Override
    public List<Laptop> findAll() throws SQLException {
        List<Laptop> laptops = new ArrayList<>();
        Connection connection = ConnectionManager.getConnection();
        try(Statement statement = connection.createStatement()){
            try(ResultSet resultSet = statement.executeQuery(FIND_ALL)){
                while (resultSet.next()){
                    laptops.add((Laptop) new Transformer<>(Laptop.class).getInstance(resultSet));
                }
            }
        }
        return laptops;
    }

    @Override
    public int create(Laptop object) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try(PreparedStatement statement = connection.prepareStatement(CREATE)){
            statement.setInt(1,object.getBarCode());
            statement.setString(2,object.getName());
            statement.setString(3,object.getModel());
            statement.setInt(4,object.getSpeed());
            statement.setInt(5,object.getRam());
            statement.setDouble(6,object.getPrice());
            statement.setInt(7,object.getScreen());
            return statement.executeUpdate();
        }
    }

    @Override
    public int update(Laptop object) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try(PreparedStatement statement = connection.prepareStatement(UPDATE)){
            statement.setDouble(1,object.getPrice());
            statement.setInt(2,object.getBarCode());
            return statement.executeUpdate();
        }
    }

    @Override
    public int delete(Integer id) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try(PreparedStatement statement = connection.prepareStatement(DELETE)){
            statement.setInt(1,id);
            return statement.executeUpdate();
        }
    }

    @Override
    public List<Laptop> showByPriceRange(double begin, double end) throws SQLException {
        List<Laptop> laptops = new ArrayList<>();
        Connection connection = ConnectionManager.getConnection();
        try(PreparedStatement statement = connection.prepareStatement(SEARCH_BY_RANGE)){
            statement.setDouble(1, begin);
            statement.setDouble(2, end);
            try(ResultSet result = statement.executeQuery()){
                while (result.next()){
                    laptops.add((Laptop) new Transformer<>(Laptop.class).getInstance(result));
                }
            }
            return laptops;
        }
    }
}
