package com.epam.controller.dao.implementations;

import com.epam.controller.Transformer;
import com.epam.controller.dao.interfaces.DeliveryDAO;
import com.epam.model.ConnectionManager;
import com.epam.model.tables.Delivery;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DeliveryDAOImpl implements DeliveryDAO {
    private static final String FIND_ALL;
    private static final String DELETE;
    private static final String CREATE;
    private static final String UPDATE;

    static {
        FIND_ALL = bundle.getString("FIND_ALL_Delivery");
        DELETE = bundle.getString("DELETE_Delivery");
        CREATE = bundle.getString("CREATE_Delivery");
        UPDATE = bundle.getString("UPDATE_Delivery");
    }

    @Override
    public List<Delivery> findAll() throws SQLException {
        List<Delivery> deliveries = new ArrayList<>();
        Connection connection = ConnectionManager.getConnection();
        try (Statement statement = connection.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(FIND_ALL)) {
                while (resultSet.next()) {
                    deliveries.add((Delivery) new Transformer<>(Delivery.class).getInstance(resultSet));
                }
            }
        }
        return deliveries;
    }

    @Override
    public int create(Delivery object) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement statement = connection.prepareStatement(CREATE)) {
            statement.setInt(1, object.getId());
            statement.setDate(2, object.getDate());
            statement.setTime(3, object.getTime());
            statement.setDouble(4, object.getPrice());
            statement.setString(5, object.getDetails());
            statement.setInt(6, object.getOrderId());
            return statement.executeUpdate();
        }
    }

    @Override
    public int update(Delivery object) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement statement = connection.prepareStatement(UPDATE)) {
            statement.setInt(3, object.getId());
            statement.setDate(1, object.getDate());
            statement.setTime(2, object.getTime());
            return statement.executeUpdate();
        }
    }

    @Override
    public int delete(Integer id) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement statement = connection.prepareStatement(DELETE)) {
            statement.setInt(1, id);
            return statement.executeUpdate();
        }
    }
}