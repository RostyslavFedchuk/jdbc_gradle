package com.epam.controller.dao.implementations;

import com.epam.controller.Transformer;
import com.epam.controller.dao.interfaces.ProductDAO;
import com.epam.model.ConnectionManager;
import com.epam.model.tables.Product;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ProductDAOImpl implements ProductDAO {
    private static final String FIND_ALL;
    private static final String DELETE;
    private static final String CREATE;
    private static final String UPDATE;
    private static final String JOIN_PRODUCT;

    static {
        FIND_ALL = bundle.getString("FIND_ALL_Product");
        DELETE = bundle.getString("DELETE_Product");
        CREATE = bundle.getString("CREATE_Product");
        UPDATE = bundle.getString("UPDATE_Product");
        JOIN_PRODUCT = bundle.getString("JOIN_PRODUCT");
    }

    @Override
    public List<Product> findAll() throws SQLException {
        List<Product> products = new ArrayList<>();
        Connection connection = ConnectionManager.getConnection();
        try (Statement statement = connection.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(FIND_ALL)) {
                while (resultSet.next()) {
                    products.add((Product) new Transformer<>(Product.class).getInstance(resultSet));
                }
            }
        }
        return products;
    }

    @Override
    public int create(Product object) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement statement = connection.prepareStatement(CREATE)) {
            statement.setInt(1, object.getId());
            statement.setInt(2, object.getBarCode());
            statement.setInt(3, object.getCategoryId());
            statement.setString(4, object.getProduction());
            return statement.executeUpdate();
        }
    }

    @Override
    public int update(Product object) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement statement = connection.prepareStatement(UPDATE)) {
            statement.setInt(1, object.getCategoryId());
            statement.setString(2, object.getProduction());
            statement.setInt(3, object.getBarCode());
            return statement.executeUpdate();
        }
    }

    @Override
    public int delete(Integer id) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement statement = connection.prepareStatement(DELETE)) {
            statement.setInt(1, id);
            return statement.executeUpdate();
        }
    }

    @Override
    public void joinTables() throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try (Statement statement = connection.createStatement()) {
            statement.executeQuery(JOIN_PRODUCT);
        }
    }
}