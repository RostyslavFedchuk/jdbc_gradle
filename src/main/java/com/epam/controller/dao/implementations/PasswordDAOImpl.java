package com.epam.controller.dao.implementations;

import com.epam.controller.Transformer;
import com.epam.controller.dao.interfaces.PasswordDAO;
import com.epam.model.ConnectionManager;
import com.epam.model.tables.Password;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class PasswordDAOImpl implements PasswordDAO {
    private static final String FIND_ALL;
    private static final String DELETE;
    private static final String CREATE;
    private static final String UPDATE;

    static {
        FIND_ALL = bundle.getString("FIND_ALL_Password");
        DELETE = bundle.getString("DELETE_Password");
        CREATE = bundle.getString("CREATE_Password");
        UPDATE = bundle.getString("UPDATE_Password");
    }

    @Override
    public List<Password> findAll() throws SQLException {
        List<Password> passwords = new ArrayList<>();
        Connection connection = ConnectionManager.getConnection();
        try (Statement statement = connection.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(FIND_ALL)) {
                while (resultSet.next()) {
                    passwords.add((Password) new Transformer<>(Password.class).getInstance(resultSet));
                }
            }
        }
        return passwords;
    }

    @Override
    public int create(Password object) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement statement = connection.prepareStatement(CREATE)) {
            statement.setInt(1, object.getId());
            statement.setString(2, object.getValue());
            return statement.executeUpdate();
        }
    }

    @Override
    public int update(Password object) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement statement = connection.prepareStatement(UPDATE)) {
            statement.setInt(2, object.getId());
            statement.setString(1, object.getValue());
            return statement.executeUpdate();
        }
    }

    @Override
    public int delete(Integer id) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement statement = connection.prepareStatement(DELETE)) {
            statement.setInt(1, id);
            return statement.executeUpdate();
        }
    }
}
