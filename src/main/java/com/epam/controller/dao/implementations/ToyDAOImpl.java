package com.epam.controller.dao.implementations;

import com.epam.controller.Transformer;
import com.epam.controller.dao.interfaces.ToyDAO;
import com.epam.model.ConnectionManager;
import com.epam.model.tables.Toy;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ToyDAOImpl implements ToyDAO {
    private static final String FIND_ALL;
    private static final String DELETE;
    private static final String CREATE;
    private static final String UPDATE;

    static {
        FIND_ALL = bundle.getString("FIND_ALL_Toy");
        DELETE = bundle.getString("DELETE_Toy");
        CREATE = bundle.getString("CREATE_Toy");
        UPDATE = bundle.getString("UPDATE_Toy");
    }

    @Override
    public List<Toy> findAll() throws SQLException {
        List<Toy> toys = new ArrayList<>();
        Connection connection = ConnectionManager.getConnection();
        try(Statement statement = connection.createStatement()){
            try(ResultSet resultSet = statement.executeQuery(FIND_ALL)){
                while (resultSet.next()){
                    toys.add((Toy) new Transformer<>(Toy.class).getInstance(resultSet));
                }
            }
        }
        return toys;
    }

    @Override
    public int create(Toy object) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try(PreparedStatement statement = connection.prepareStatement(CREATE)){
            statement.setInt(1,object.getBarCode());
            statement.setString(2,object.getName());
            statement.setString(3,object.getColor());
            statement.setDouble(4,object.getPrice());
            return statement.executeUpdate();
        }
    }

    @Override
    public int update(Toy object) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try(PreparedStatement statement = connection.prepareStatement(UPDATE)){
            statement.setDouble(1,object.getPrice());
            statement.setInt(2,object.getBarCode());
            return statement.executeUpdate();
        }
    }

    @Override
    public int delete(Integer id) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try(PreparedStatement statement = connection.prepareStatement(DELETE)){
            statement.setInt(1,id);
            return statement.executeUpdate();
        }
    }
}
