package com.epam.controller.dao.implementations;

import com.epam.controller.Transformer;
import com.epam.controller.dao.interfaces.StatusDAO;
import com.epam.model.ConnectionManager;
import com.epam.model.tables.Status;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class StatusDAOImpl implements StatusDAO {
    private static final String FIND_ALL;
    private static final String DELETE;
    private static final String CREATE;
    private static final String UPDATE;

    static {
        FIND_ALL = bundle.getString("FIND_ALL_Status");
        DELETE = bundle.getString("DELETE_Status");
        CREATE = bundle.getString("CREATE_Status");
        UPDATE = bundle.getString("UPDATE_Status");
    }

    @Override
    public List<Status> findAll() throws SQLException {
        List<Status> statuses = new ArrayList<>();
        Connection connection = ConnectionManager.getConnection();
        try(Statement statement = connection.createStatement()){
            try(ResultSet resultSet = statement.executeQuery(FIND_ALL)){
                while (resultSet.next()){
                    statuses.add((Status) new Transformer<>(Status.class).getInstance(resultSet));
                }
            }
        }
        return statuses;
    }

    @Override
    public int create(Status object) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try(PreparedStatement statement = connection.prepareStatement(CREATE)){
            statement.setInt(1,object.getId());
            statement.setString(2,object.getName());
            statement.setString(3,object.getDescription());
            return statement.executeUpdate();
        }
    }

    @Override
    public int update(Status object) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try(PreparedStatement statement = connection.prepareStatement(UPDATE)){
            statement.setString(1,object.getDescription());
            statement.setString(2,object.getName());
            return statement.executeUpdate();
        }
    }

    @Override
    public int delete(Integer id) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try(PreparedStatement statement = connection.prepareStatement(DELETE)){
            statement.setInt(1,id);
            return statement.executeUpdate();
        }
    }
}
