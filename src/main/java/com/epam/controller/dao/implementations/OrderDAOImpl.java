package com.epam.controller.dao.implementations;

import com.epam.controller.Transformer;
import com.epam.controller.dao.interfaces.OrderDAO;
import com.epam.model.ConnectionManager;
import com.epam.model.tables.Order;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class OrderDAOImpl implements OrderDAO {
    private static final String FIND_ALL;
    private static final String DELETE;
    private static final String CREATE;
    private static final String UPDATE;

    static {
        FIND_ALL = bundle.getString("FIND_ALL_Order");
        DELETE = bundle.getString("DELETE_Order");
        CREATE = bundle.getString("CREATE_Order");
        UPDATE = bundle.getString("UPDATE_Order");
    }

    @Override
    public List<Order> findAll() throws SQLException {
        List<Order> orders = new ArrayList<>();
        Connection connection = ConnectionManager.getConnection();
        try (Statement statement = connection.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(FIND_ALL)) {
                while (resultSet.next()) {
                    orders.add((Order) new Transformer(Order.class).getInstance(resultSet));
                }
            }
        }
        return orders;
    }


    @Override
    public int create(Order object) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(CREATE)) {
            ps.setInt(1,object.getId());
            ps.setDate(2,object.getDate());
            ps.setString(3,object.getDetails());
            ps.setInt(4,object.getStatusCode());
            ps.setInt(5,object.getUserId());
            ps.setInt(6,object.getPaymentId());
            return ps.executeUpdate();
        }
    }

    @Override
    public int update(Order object) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(UPDATE)) {
            ps.setString(1,object.getDetails());
            ps.setInt(2,object.getId());
            return ps.executeUpdate();
        }
    }

    @Override
    public int delete(Integer id) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(DELETE)) {
            ps.setInt(1,id);
            return ps.executeUpdate();
        }
    }
}
