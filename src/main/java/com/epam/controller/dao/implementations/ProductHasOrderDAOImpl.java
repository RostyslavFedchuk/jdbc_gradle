package com.epam.controller.dao.implementations;

import com.epam.controller.Transformer;
import com.epam.controller.dao.interfaces.ProductHasOrderDAO;
import com.epam.model.ConnectionManager;
import com.epam.model.tables.ProductHasOrder;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ProductHasOrderDAOImpl implements ProductHasOrderDAO {
    private static final String FIND_ALL;
    private static final String DELETE;
    private static final String CREATE;
    private static final String UPDATE;
    private static final String DELETE_PRODUCTS_BY_ORDER;

    static {
        FIND_ALL = bundle.getString("FIND_ALL_ProductHasOrder");
        DELETE = bundle.getString("DELETE_ProductHasOrder");
        CREATE = bundle.getString("CREATE_ProductHasOrder");
        UPDATE = bundle.getString("UPDATE_ProductHasOrder");
        DELETE_PRODUCTS_BY_ORDER = bundle.getString("DELETE_PRODUCTS_BY_ORDER");
    }

    @Override
    public List<ProductHasOrder> findAll() throws SQLException {
        List<ProductHasOrder> list = new ArrayList<>();
        Connection connection = ConnectionManager.getConnection();
        try (Statement statement = connection.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(FIND_ALL)) {
                while (resultSet.next()) {
                    list.add((ProductHasOrder) new Transformer<>(ProductHasOrder.class).getInstance(resultSet));
                }
            }
        }
        return list;
    }

    @Override
    public int create(ProductHasOrder object) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement statement = connection.prepareStatement(CREATE)) {
            statement.setInt(1, object.getId());
            statement.setInt(2, object.getProductId());
            statement.setInt(3, object.getOrderId());
            return statement.executeUpdate();
        }
    }

    @Override
    public int update(ProductHasOrder object) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement statement = connection.prepareStatement(UPDATE)) {
            statement.setInt(1, object.getProductId());
            statement.setInt(2, object.getOrderId());
            statement.setInt(3, object.getId());
            return statement.executeUpdate();
        }
    }

    @Override
    public int delete(Integer id) throws SQLException {
        return remove(id, DELETE);
    }

    @Override
    public int deleteProductsByOrder(Integer id) throws SQLException {
        return remove(id, DELETE_PRODUCTS_BY_ORDER);
    }

    private int remove(Integer id, String query)  throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setInt(1, id);
            return statement.executeUpdate();
        }
    }
}
