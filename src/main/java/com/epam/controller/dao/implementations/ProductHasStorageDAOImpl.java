package com.epam.controller.dao.implementations;

import com.epam.controller.Transformer;
import com.epam.controller.dao.interfaces.ProductHasStorageDAO;
import com.epam.model.ConnectionManager;
import com.epam.model.tables.ProductHasStorage;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ProductHasStorageDAOImpl implements ProductHasStorageDAO {
    private static final String FIND_ALL;
    private static final String DELETE;
    private static final String CREATE;
    private static final String UPDATE;

    static {
        FIND_ALL = bundle.getString("FIND_ALL_ProductHasStorage");
        DELETE = bundle.getString("DELETE_ProductHasStorage");
        CREATE = bundle.getString("CREATE_ProductHasStorage");
        UPDATE = bundle.getString("UPDATE_ProductHasStorage");
    }

    @Override
    public List<ProductHasStorage> findAll() throws SQLException {
        List<ProductHasStorage> list = new ArrayList<>();
        Connection connection = ConnectionManager.getConnection();
        try (Statement statement = connection.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(FIND_ALL)) {
                while (resultSet.next()) {
                    list.add((ProductHasStorage) new Transformer<>(ProductHasStorage.class).getInstance(resultSet));
                }
            }
        }
        return list;
    }

    @Override
    public int create(ProductHasStorage object) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement statement = connection.prepareStatement(CREATE)) {
            statement.setInt(1, object.getId());
            statement.setInt(2, object.getProductId());
            statement.setInt(3, object.getStorageId());
            statement.setInt(4, object.getCount());
            return statement.executeUpdate();
        }
    }

    @Override
    public int update(ProductHasStorage object) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement statement = connection.prepareStatement(UPDATE)) {
            statement.setInt(1, object.getCount());
            statement.setInt(2, object.getProductId());
            statement.setInt(3, object.getStorageId());
            statement.setInt(4, object.getId());
            return statement.executeUpdate();
        }
    }

    @Override
    public int delete(Integer id) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement statement = connection.prepareStatement(DELETE)) {
            statement.setInt(1, id);
            return statement.executeUpdate();
        }
    }
}

