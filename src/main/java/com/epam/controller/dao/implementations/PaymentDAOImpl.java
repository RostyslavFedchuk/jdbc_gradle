package com.epam.controller.dao.implementations;

import com.epam.controller.Transformer;
import com.epam.controller.dao.interfaces.PaymentDAO;
import com.epam.model.ConnectionManager;
import com.epam.model.tables.Payment;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class PaymentDAOImpl implements PaymentDAO {
    private static final String FIND_ALL;
    private static final String DELETE;
    private static final String CREATE;
    private static final String UPDATE;

    static {
        FIND_ALL = bundle.getString("FIND_ALL_Payment");
        DELETE = bundle.getString("DELETE_Payment");
        CREATE = bundle.getString("CREATE_Payment");
        UPDATE = bundle.getString("UPDATE_Payment");
    }

    @Override
    public List<Payment> findAll() throws SQLException {
        List<Payment> payments = new ArrayList<>();
        Connection connection = ConnectionManager.getConnection();
        try (Statement statement = connection.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(FIND_ALL)) {
                while (resultSet.next()) {
                    payments.add((Payment) new Transformer<>(Payment.class).getInstance(resultSet));
                }
            }
        }
        return payments;
    }

    @Override
    public int create(Payment object) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement statement = connection.prepareStatement(CREATE)) {
            statement.setInt(1, object.getId());
            statement.setString(2, object.getCredit_card_number());
            statement.setString(3, object.getDetails());
            return statement.executeUpdate();
        }
    }

    @Override
    public int update(Payment object) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement statement = connection.prepareStatement(UPDATE)) {
            statement.setString(1, object.getCredit_card_number());
            statement.setString(2, object.getDetails());
            statement.setInt(3, object.getId());
            return statement.executeUpdate();
        }
    }

    @Override
    public int delete(Integer id) throws SQLException {
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement statement = connection.prepareStatement(DELETE)) {
            statement.setInt(1, id);
            return statement.executeUpdate();
        }
    }
}