package com.epam.controller;

import com.epam.model.ConnectionManager;
import com.epam.model.structure.ColumnStructure;
import com.epam.model.structure.TableStructure;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DBStructure {

    public static List<TableStructure> getAllTables() throws SQLException {
        List<TableStructure> tables = new ArrayList<>();
        Connection connection = ConnectionManager.getConnection();
        DatabaseMetaData dbMeta = connection.getMetaData();
        ResultSet result = dbMeta.getTables(connection.getCatalog(),
                null, "%", new String[]{"TABLE"});
        while(result.next()){
            String tableName = result.getString("TABLE_NAME");
            TableStructure table = new TableStructure();
            table.setName(tableName);
            table.setColumns(getColumns(connection, dbMeta, tableName));
            tables.add(table);
        }
        return tables;
    }

    private static List<ColumnStructure> getColumns(Connection connection, DatabaseMetaData dbMeta, String tableName) throws SQLException {
        List<ColumnStructure> currentColumns = new ArrayList<>();
        ResultSet columnsSet = dbMeta.getColumns(connection.getCatalog(),
                null, tableName, "%");
        while (columnsSet.next()){
            ColumnStructure columns = new ColumnStructure();
            columns.setName(columnsSet.getString("COLUMN_NAME"));
            columns.setType(columnsSet.getString("TYPE_NAME"));
            columns.setSize(columnsSet.getString("COLUMN_SIZE"));
            columns.setNull(columnsSet.getString("IS_NULLABLE").equals("YES"));
            columns.setAutoincrement(columnsSet.getString("IS_AUTOINCREMENT").equals("YES"));
            setKeys(connection, dbMeta, tableName, columns);
            currentColumns.add(columns);
        }
        return currentColumns;
    }

    private static void setKeys(Connection connection, DatabaseMetaData dbMeta, String tableName, ColumnStructure columns)
            throws SQLException {
        ResultSet primaryKeys = dbMeta.getPrimaryKeys(connection.getCatalog(), null, tableName);
        while (primaryKeys.next()) {
            if (columns.getName().equals(primaryKeys.getString("COLUMN_NAME"))) {
                columns.setPrimaryKey(true);
                break;
            }
        }

        ResultSet foreignKeys = dbMeta.getImportedKeys(connection.getCatalog(), null, tableName);
        while (foreignKeys.next()) {
            if (columns.getName().equals(foreignKeys.getString("FKCOLUMN_NAME"))) {
                columns.setForeignKey(true);
                break;
            }
        }
    }

}
